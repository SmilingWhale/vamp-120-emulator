// Javascript
// исходное состояние 8 переключателей на устройстве
var Switches = [true, true, true, false, false, true, true, true];

// бит работы установки
var v120 = false;

// зажатие кнопки контроля по току
var IButton = false;

// защелки выходных реле Т1 и Т2
var T1Latch = true;
var T2Latch = true;

var S1S2 = true; // ************** а разве не S2S3 ???
var T1T2 = false;

// срабатывание датчиков дуговой защиты
var Sensors = [false, false, false, false]

// состояние светодиодов блока проверочной установки.
var Leds = [false, false, false, false, false]

function clickSW(z) {
	if (v120) {
		document.getElementById('SW_On').style.fill = "#000000";
		document.getElementById('SW_On').style.opacity = "1.0";
		document.getElementById('SW_Off').style.fill = "#FFFFFF";
		document.getElementById('SW_Off').style.opacity = "0.0";

		document.getElementById('PowerLED').style.opacity = "0.5";

		resetSensors();

		Leds[0] = false;
		document.getElementById('Cover').style.display = "none";
	} else {
		document.getElementById('SW_On').style.fill = "#FFFFFF";
		document.getElementById('SW_On').style.opacity = "0.0";
		document.getElementById('SW_Off').style.fill = "#000000";
		document.getElementById('SW_Off').style.opacity = "1.0";

		document.getElementById('PowerLED').style.opacity = "0.0";

		Leds[0] = true;
		document.getElementById('Cover').style.display = "inline";
	}
	v120 = !v120;

	document.getElementById('resetButton').style.opacity = RBS();
	document.getElementById('I_Button').style.opacity = IBS();
	document.getElementById('SF_Lamp').style.fill = SFL();

	updateInfo();
}

function resetSensors() {

	for (i = 0; i < 4; i++) {
		Sensors[i] = false;
		Leds[i] = false;
	}

	Leds[4] = false;
	//updateInfo();	
}

function InitSwitches() {
	for (i = 1; i <= 8; i++) {
		redrawSwitches(i);
	}

	document.getElementById('SW_On').style.fill = "#000000";
	document.getElementById('SW_On').style.opacity = "1.0";
	document.getElementById('SW_Off').style.fill = "#FFFFFF";
	document.getElementById('SW_Off').style.opacity = "0.0";
	document.getElementById('PowerLED').style.opacity = "0.5";
}

function SWClick(z) { // --- переписать, чтобы обнулялось при отключении датчика...

	Switches[z - 1] = !Switches[z - 1];

	if (z >= 1) {
		if (Switches[z - 1]) {
			Sensors[z] = false;
		}

	}

	switch (z) {
		case 4:
			T1T2 = !T1T2;
			break;
		case 6:
			T1Latch = !T1Latch;
			break;
		case 7:
			T2Latch = !T2Latch;
			break;
		case 8:
			S1S2 = !S1S2;
			break;
		default:
			break;
	}

	redrawSwitches(z);
	updateInfo();
}

function redrawSwitches(i) {
	if (!v120) {
		var s = '';
		if (Switches[i - 1]) {
			document.getElementById('SW' + i + '_On').style.fill = "#000000";
			document.getElementById('SW' + i + '_Off').style.fill = "#FFFFFF";
		} else {
			document.getElementById('SW' + i + '_On').style.fill = "#FFFFFF";
			document.getElementById('SW' + i + '_Off').style.fill = "#000000";
		}

		// for (i = 0; i < 8; i++) {
		// 	if (Switches[i]) {
		// 		s = s + '1';
		// 	} else {
		// 		s = s + '0';
		// 	}
		// }
		// document.getElementById('Switches').innerHTML = s;
	}
}

function clickOnButton(z) {
	switch (z) {
		case 1:
			resetSensors();
			break;
		case 2:
			resetSensors();
			document.getElementById('resetButton').style.opacity = "0.0";
			break;
		case 3:
			document.getElementById('resetButton').style.opacity = RBS();
			break;
		case 4:
			IButton = !IButton;
			document.getElementById('I_Button').style.opacity = IBS();
			break;
		case 5: // --- это не нужно, запутка
			T1Latch = !T1Latch;
			break;
		case 6:
			T2Latch = !T2Latch;
			break;
		default:
			break;
	}
	updateInfo();
}

function updateInfo() {
	var s = '';
	var s1 = '';
	var a = false;
	// document.getElementById('isens').innerHTML = 'v120:' + v120 + '  I>>:' + IButton + '   T1Latch:' + T1Latch + '   T2Latch:' + T2Latch + '  T1T2:' + T1T2 + '  S1S2' + S1S2;

	for (i = 1; i < 5; i++) {
		if (Sensors[i - 1]) {

			s = s + '1';
			s1 = 'Sensor' + i + '_LED';
			document.getElementById(s1).style.opacity = "0.0";
		} else {
			s = s + '0';
			s1 = 'Sensor' + i + '_LED';
			document.getElementById(s1).style.opacity = "0.5";
		}
	}


	if (Leds[3]) {
		document.getElementById('Trip1_LED').style.fill = "#FFA520";
		document.getElementById('Trip1_LED').style.opacity = "0.7";
	} else {
		document.getElementById('Trip1_LED').style.fill = "#999999";
		document.getElementById('Trip1_LED').style.opacity = "0.5";
	}
	if (Leds[1]) {
		document.getElementById('Trip2_LED').style.fill = "#DAA520";
		document.getElementById('Trip2_LED').style.opacity = "0.7"
	} else {
		document.getElementById('Trip2_LED').style.fill = "#999999";
		document.getElementById('Trip2_LED').style.opacity = "0.5"
	}

	// document.getElementById('sensorss').innerHTML = s;


	document.getElementById('TripAlarm_Lamp').style.fill = TAL();
	document.getElementById('Trip2_Lamp').style.fill = T2L();
	document.getElementById('Trip1_Lamp').style.fill = T1L();
	document.getElementById('TripSignal_Lamp').style.fill = TSL();
}

function IBS() {
	var op = "1.0";

	if (IButton) {
		op = "0.0";
	} else {
		if (v120) {
			op = "0.85"
		}
	}
	return op;
}

function RBS() {
	var op = "0.85";
	if (!v120) {
		op = "1.0";
	}
	return op;
}

function SFL() {
	var op = "#90EE90";
	if (!Leds[0]) {
		op = "#999999";
	}
	return op;
}

function T1L() {
	var op = "#999999";
	if (Leds[3]) {
		op = "#FF0000";
	}
	return op;
}

function T2L() {
	var op = "#999999";
	if (Leds[1]) {
		op = "#FF0000";
	}
	return op;
}

function TAL() {
	var op = "#999999";
	if (Leds[2]) {
		op = "#FF0000";
	}
	return op;
}

function TSL() {
	var op = "#999999";
	if (Leds[4]) {
		op = "#FF0000";
	}
	return op;
}

function clickOnSensor(z) {
// обработка нажатия по какому-либо датчику дуговой защиты
	if (v120) {
		if ((Switches[4]) || (!Switches[4] && IButton)) {
			switch (z) {
				case 1:
					Sensors[0] = true;

					Leds[2] = true;
					Leds[3] = true;
					Leds[4] = true;

					if (T1T2) {
						Leds[1] = true;
					}

					break;
				case 2:

					if (!T1Latch) {
						//Leds[3]=false;
						Leds[4] = false;
					}
					if (T1T2 & !T2Latch) {
						//Leds[1]=false;		
					}
					if (!T1Latch & !Leds[1]) {
						Leds[2] = false;
					}
					break;
				case 3:
					Sensors[1] = true;

					if (!Switches[0]) {
						Leds[2] = true;
						Leds[3] = true;
						Leds[4] = true;
						if (S1S2 | T1T2) {
							Leds[1] = true;
						}
					}
					break;
				case 4:
					if (!T1Latch) {
						Leds[4] = false;
						//Leds[3]=false;
					}
					if ((S1S2 | T1T2) & !T2Latch) {
						Leds[1] = false;
					}
					if (!T1Latch & !Leds[1]) {
						Leds[2] = false;
					}
					break;
				case 5:
					Sensors[2] = true;

					if (!Switches[1]) {
						Leds[2] = true;
						Leds[1] = true;

						if (S1S2 | T1T2) {
							Leds[3] = true;
							Leds[4] = true;
						}
					}

					break;
				case 6:
					if (!T2Latch) {
						Leds[1] = false;
					}
					if (!T1Latch & !T2Latch) {
						Leds[2] = false;
					}
					if (!T1Latch & (S1S2 | T1T2)) {
						Leds[3] = false;
						Leds[4] = false;
					}

					break;
				case 7:
					Sensors[3] = true;

					if (!Switches[2]) {
						Leds[2] = true;
						Leds[1] = true;
						if (T1T2) {
							Leds[3] = true;
							Leds[4] = true;
						}
					}
					break;
				case 8:

					if (!T2Latch) {
						Leds[1] = false;
					}
					if (T1T2 & !T1Latch) {
						Leds[3] = false;
						Leds[4] = false;
					}
					if (!T2Latch & !Leds[3]) {
						Leds[2] = false;
					}
					break;
				default:
					break;
			}

			updateInfo();
		}
	}
}